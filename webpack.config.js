const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
  mode: 'production',
  devtool: '#source-map',
  entry: {
    background: './src/background.js',
      app: './src/js/app.jsx'
  },
  resolve: {
    extensions: ['.js', '.json', '.jsx'],
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js',
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules\/(?!metamask-extension)/,
        loader: 'babel-loader',
        options:{
            presets:["env", "es2016", "react", 'stage-0']
        }
      },
      {
        test: /\.(png|jpg|gif)$/,
        loader: 'url-loader',
      },
        {
            test: /\.(eot|svg|ttf|woff|woff2)$/,
            loader: "url-loader",
        },
        {
            test: /\.scss$/,
            use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: ['css-loader', 'sass-loader']
            })
        }
    ],
  },
  plugins: [
      new ExtractTextPlugin("[name].css"),
      new CopyWebpackPlugin([
        './src/manifest.json',
        { from: './src/html' },
        { from: './src/i', to: "./assets/i" },
  ])],
};
