

# TODO:
~~~
1.Fish plugin :D 
    - Loader, before get fina lstatus
    - Update header
    - On submit form, redirect to dapp with get params
    - Empty form after submit with redirect
    - Add new status (all pendings) (Verification in progress)
      - btn "see progress"
    - If empty DB
      - btn "send report"
    - Check if i can open extension from background.js  
2.Start dapp :DD
    - In progress
3. Finish it xDDD
~~~



# Envoirment:
~~~
$ sudo add-apt-repository -y ppa:ethereum/ethereum // Installing Go Ethereum
$ sudo apt-get update
$ sudo apt-get install ethereum
$ sudo npm install -g solc // Installing Solidity
$ npm install -g ethereumjs-testrpc
$ sudo npm install -g truffle //install Truffle
$ sudo npm install zeppelin-solidity| // install the Zeppelin library
~~~

### Локальная тестовая сеть блокчейн geth:
~~~
$ geth --rpc --dev  --rpccorsdomain "localhost" --rpcport "8545" --rpcaddr "127.0.0.1" --rpcapi "admin,debug,miner,shh,txpool,db,eth,net,web3,personal" console 2>>geth.log
~~~
Флаг --testnet подключает нас к тестовой сети Ropsten. Для подключения к Rinkeby используйте вместо этого флаг --rinkeby. Дополнительно мы перенаправляем сообщения из стандартного потока (stderr) geth’а в файл geth.log, иначе они будут мешаться в консоли.
~~~
geth console --testnet 2>>geth.log
~~~

### Локальная тестовая сеть блокчейн testrpc:
~~~
$ testrpc
EthereumJS TestRPC v4.1.3 (ganache-core: 1.1.3)

Available Accounts
==================
(0) 0x875c535317cc48ce2aff1d2b0a0fb0e8fff4e206
(1) 0xefa1653d91d302d2bb8a839d9fd2c34a45259141
(2) 0xf065b14b3a957580bca106b948b1c2ecb4143d18
(3) 0x21a49ff2ba2a23475cbba7e3191a6782fcdd01bf
(4) 0xd58c9a31db939dd9d21c2aaf1482e3ab5e070062
(5) 0x41ff0daeabe56c578bcdf6b19c3dd0ffd11f92d9
(6) 0xc75cd461408543dcc15b4d6e52b9196fccd4e4c4
(7) 0x311480e2aa6d29d55e253254baaf3fb61e54ea57
(8) 0xf2350044310cb502c2b829b98bbd54b63b05e301
(9) 0x8f374b52a3529f6a4c81ece082e12613a31a956b

Private Keys
==================
(0) fd7778d8c455858d8bfc601638eeebcd43ca5346e28e598f1e436cf7d575deed
(1) 73c2f49a4b1895955486be6686fa5d2314ba86e115826a7e6f5827c7db5965e3
(2) d93cbaac4fd8f6fc5de8b4844d6969603b0fbd212956c2abe0d00c1f074b6b35
(3) 68ae27b1d72479192bb617759d3b045f28a78eb8be99d758c0d17f728bfca041
(4) 540be5f52bcd2d3212dea6dac51ae3f2b7e65647861b0461710e5c88f8cf1d4c
(5) 43386f9b589a314c5ae7b7669faa6ae9fc02e7965447166756e15a100a148f12
(6) 39d145b2ebd8f26306234472dbe4f4135358062de051e2ff3a73d50e80613193
(7) 8a3f1f0356b9c8d2b9a1d76a60ff8f68febcec6690a329f68ac66e2bc8e9594b
(8) 8545f3ab0d72de19479facba7862aef236c68a0d7d8801bd0cfb0d2b295c89ee
(9) 6825fb179dc58cd72b7edc5e03066d9d2505a2fd5b7d4db18f4cb71ee5893956

HD Wallet
==================
Mnemonic:      ribbon cigar vehicle demand visit combine manage resource legal ketchup able divide
Base HD Path:  m/44'/60'/0'/0/{account_index}

Listening on localhost:8545
~~~

### Примеры команд:
~~~
> personal.newAccount("123") // Создаем новый аккаунт с паролем "123"
> eth.accounts // Смотрим список аккаунтов
> miner.setEtherbase(eth.accounts[0]) // Устанавливаем его в качестве аккаунта для майнинг
> eth.coinbase // Проверяем
> miner.start() // запускаю майнер
> miner.stop()
> eth.getBalance(eth.coinbase) // Проверим баланс
> web3.personal.unlockAccount(eth.accounts[0], "123") // разблокирока аккаунта
> loadScript("geth_scripts.js")
~~~

~~~
// geth console
> var source = 'contract Multiply7 { event Print(uint); function multiply(uint input) returns (uint) { Print(input * 7); return input * 7; } }';
> var compiled = web3.eth.compile.solidity(source);
~~~

### Команды truffle
~~~
$ cd /to/path/project/
$ truffle init
$ truffle compile
$ truffle create contract Example
$ truffle migrate
$ truffle deploy
$ truffle console
> web3.eth.accounts
> web3.personal.unlockAccount(web3.eth.accounts[0], "123")
~~~

Урл для запросов из Postman к RPC Api: `http://localhost:8545`


### Компилим solidity в консоли используя solc

~~~
$ solc --bin --abi  UselessWorker.sol
~~~
Флагами --bin и --abi говорим компилятору генерировать бинарный код и интерфейс, На команду выдается ответ подобный следующему:
~~~
======= UselessWorker.sol:UselessWorker =======
Binary:
606060405260008055341561001357600080fd5b5b60fd806100226000396000f30060606040526000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff1680637ec94a1314604757806397286e7114606d575b600080fd5b3415605157600080fd5b6057608d565b6040518082815260200191505060405180910390f35b3415607757600080fd5b608b60048080359060200190919050506093565b005b60005481565b600081600081905550600090505b8181121560cc5780604051808281526020019150506040518091039050505b808060010191505060a1565b5b50505600a165627a7a72305820b0949297821556e9ed7f4941b7ae793486db6ee48e86486dc58fa3040b224d160029
Contract JSON ABI
[{"constant":true,"inputs":[],"name":"successfullyExecutedIterations","outputs":[{"name":"","type":"int256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_iterations","type":"int256"}],"name":"doWork","outputs":[],"payable":false,"type":"function"}]
~~~

#### Деплой контракта из консоли
~~~
$ geth --rpc --dev  --rpccorsdomain "localhost" --rpcport "8545" --rpcaddr "127.0.0.1" --rpcapi "admin,debug,miner,shh,txpool,db,eth,net,web3,personal" console 2>>geth.log
> web3.fromWei(eth.getBalance(eth.coinbase));
> web3.personal.unlockAccount(eth.coinbase);
> var address = "0xd3e1c8f096e521e9a8cdb7da9908d15b1ca06b6c"
> var bin = "0x60606040525b5b60008054600160a060020a03191633600160a060020a03161790555b60018054600160a060020a03191633600160a060020a03161790555b5b6103f68061004e6000396000f300606060405263ffffffff7c0100000000000000000000000000000000000000000000000000000000600035041663368b8772811461006957806341c0e1b5146100bc5780638da5cb5b146100d1578063ce6d41de14610100578063f2fde38b1461018b575b600080fd5b341561007457600080fd5b6100ba60046024813581810190830135806020601f820181900481020160405190810160405281815292919060208401838380828437509496506101ac95505050505050565b005b34156100c757600080fd5b6100ba6101e0565b005b34156100dc57600080fd5b6100e4610208565b604051600160a060020a03909116815260200160405180910390f35b341561010b57600080fd5b610113610217565b60405160208082528190810183818151815260200191508051906020019080838360005b838110156101505780820151818401525b602001610137565b50505050905090810190601f16801561017d5780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b341561019657600080fd5b6100ba600160a060020a03600435166102c0565b005b60005433600160a060020a039081169116146101c757600080fd5b60028180516101da929160200190610318565b505b5b50565b60015433600160a060020a039081169116141561020557600154600160a060020a0316ff5b5b565b600054600160a060020a031681565b61021f610397565b60028054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156102b55780601f1061028a576101008083540402835291602001916102b5565b820191906000526020600020905b81548152906001019060200180831161029857829003601f168201915b505050505090505b90565b60005433600160a060020a039081169116146102db57600080fd5b600160a060020a038116156101dc576000805473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a0383161790555b5b5b50565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f1061035957805160ff1916838001178555610386565b82800160010185558215610386579182015b8281111561038657825182559160200191906001019061036b565b5b506103939291506103a9565b5090565b60206040519081016040526000815290565b6102bd91905b8082111561039357600081556001016103af565b5090565b905600a165627a7a723058208484c629ffae8a493727932575e574c8d155cf838e96fbb3b8d0f61f81714ede0029";
undefined
>   var abi = [{"constant":false,"inputs":[{"name":"newString","type":"string"}],"name":"setMessage","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[],"name":"kill","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"getMessage","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"payable":false,"type":"function"}];
undefined
> var contract = web3.eth.contract(abi);
> var myContract = contract.new( {from: eth.coinbase, data: bin, gas: 1000000}, function(e, contract) { if (e) { console.log(e); } else { if (contract.address) { console.log ("mined " + contract.address);  } } });
> miner.start()
null
> mined 0xf4ae5c432f1b6b75779723a4bfef628938dcab45
> miner.stop()
true
> myContract.getMessage();
""
> var testContract = contract.at("0xf4AE5c432F1B6b75779723A4bFef628938DcaB45")
undefined
> testContract.getMessage();
""
> web3.personal.unlockAccount(eth.coinbase);
Unlock account 0xd3e1c8f096e521e9a8cdb7da9908d15b1ca06b6c
Passphrase:
> testContract.getMessage();
""
> web3.personal.unlockAccount(eth.coinbase);
Unlock account 0xd3e1c8f096e521e9a8cdb7da9908d15b1ca06b6c
Passphrase:
true
> testContract.setMessage("Hello my baby, hello my honey!", {from: eth.coinbase});
"0x20d6dcf299754edeb36a949bdd6238132adc18f5b210bc64eb59af1c3d80ee6b"
> web3.eth.getTransaction("0x20d6dcf299754edeb36a949bdd6238132adc18f5b210bc64eb59af1c3d80ee6b");
{
  blockHash: "0x0000000000000000000000000000000000000000000000000000000000000000",
  blockNumber: null,
  from: "0xd3e1c8f096e521e9a8cdb7da9908d15b1ca06b6c",
  gas: 90000,
  gasPrice: 0,
  hash: "0x20d6dcf299754edeb36a949bdd6238132adc18f5b210bc64eb59af1c3d80ee6b",
  input: "0x368b87720000000000000000000000000000000000000000000000000000000000000020000000000000000000000000000000000000000000000000000000000000001e48656c6c6f206d7920626162792c2068656c6c6f206d7920686f6e6579210000",
  nonce: 1,
  r: "0xcd214cd3debdb16cfb8485676534eec34dce953bda92a14f65e7782879c85960",
  s: "0x483cce20a0b0e6eecde6e2f05ce9dae923af208056562886d0ce7d9f740c503d",
  to: "0xf4ae5c432f1b6b75779723a4bfef628938dcab45",
  transactionIndex: 0,
  v: "0xa96",
  value: 0
}
> miner.start()
null
> miner.stop()
true
> testContract.getMessage();
"Hello my baby, hello my honey!"
> myContract.getMessage();
"Hello my baby, hello my honey!"
>
~~~
