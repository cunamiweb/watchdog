import Web3 from 'web3';
import ChromeExtension from './js/classes/ChromeExtension';
import {legitAddress, legitAbi} from './contracts/LegitContract.js';
import {reportAddress, reportAbi} from './contracts/ReportContract.js';
import {getUrlParts, getScanStatus, scanResult, setIconWithIteration} from './js/helpers';

let _domain = null;
const ex = new ChromeExtension();

const account = '0x0000000000000000000000000000000000000000';
const web3 = new Web3(new Web3.providers.HttpProvider("https://ropsten.infura.io/rMMi1mfzFv0GB8u6A8Sl"));
const legitContract = new web3.eth.Contract(legitAbi, legitAddress);
const reportContract = new web3.eth.Contract(reportAbi, reportAddress);


ex.onInstalled(() => {
    ex.setStorage({protection_status: true});
});

ex.onUpdateAvailable(() => {
    ex.setStorage({protection_status: true});
});
ex.onActivated(() => {

    ex.getSelected(null, (currentTab) => {
        _domain = getUrlParts(currentTab.url);
        getRecords();
    });
});

ex.onUpdated((tabId, changeInfo) => {
    ex.getSelected(null, (currentTab) => {
        if (changeInfo.status === 'complete') {
            _domain = getUrlParts(currentTab.url);
            getRecords();
        }
    });
});

ex.onMessage((message, sender, sendResponse) => {
    switch (message.action) {
        case "popup_opened":
            getRecords();
            break;
    }
});


function getRecords() {

    ex.sendRequest("report_url", _domain);

    if (_domain == '') {
        ex.sendRequest("change_page", {'page': 'home'});
    } else {

        ex.getStorage(['protection_status'], (result) => {

            if (result.protection_status) {

                reportContract.methods.getListReports(_domain).call({from: account}, (reportError, reportList) => {
                    legitContract.methods.getListReports(_domain).call({from: account}, (legitError, legitList) => {

                        if (reportList && legitList) {

                            let result = setResult(reportList, legitList);
                            ex.sendRequest("scan_result", result);
                            ex.sendRequest("change_page", {'page': 'result'});
                            setIconWithIteration(scanResult(result));

                        } else {
                            ex.sendRequest("change_page", {'page': 'home'});
                        }
                    });
                });
            }
        });
    }
}

function setResult(report, legit) {

    return {
        'currentUrl': _domain,
        'reportStatuses': {
            'pending': getScanStatus(report, '0'),
            'disapproved': getScanStatus(report, '1'),
            'approved': getScanStatus(report, '2')
        },
        'legitStatuses': {
            'pending': getScanStatus(legit, '0'),
            'disapproved': getScanStatus(legit, '1'),
            'approved': getScanStatus(legit, '2')
        }
    };
}