import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import store from './store';
import '../sass/style.scss'

import Content from './components/Content.jsx';
import Footer from './components/Footer.jsx';
import Home from './components/Home.jsx';
import Result from './components/Result.jsx';
import Scanner from './components/Scanner.jsx';
import Verify from './components/Verify.jsx';
import ReportScam from './components/ReportScam.jsx';
import ReportLegit from './components/ReportLegit.jsx';
import Choose from './components/Choose.jsx';

import Background from './classes/Background';

const BG = new Background();
BG.sendMessage('popup_opened');

let reportType = [{type: 'scam', name: 'Scam'}, {type: 'fishing', name: 'Fishing'}];

ReactDOM.render(
    <Provider store={store}>
        <div className="container">
            <Content>
                <Home key="home"/>
                <ReportScam key="report" reportType={reportType}/>
                <ReportLegit key="legit"/>
                <Choose key="choose"/>
                <Result key="result"/>
                <Scanner key="scanner"/>
                <Verify key="verify"/>
            </Content>
            <Footer/>
        </div>
    </Provider>,
    document.getElementById('main')
);