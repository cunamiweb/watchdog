function setPage(page) {
    return {
        type: "SET_PAGE",
        page
    }
}

function setScanResult(data) {
    return {
        type: "SET_SCAN_RESULT",
        data
    }
}

function setReportUrl(data) {
    return {
        type: "SET_REPORT_URL",
        data
    }
}

function setProtection(protection) {
    return {
        type: "SET_PROTECTION",
        protection
    }
}

export {setPage, setProtection, setScanResult, setReportUrl};