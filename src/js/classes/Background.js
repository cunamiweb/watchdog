'use strict';

import _ from 'lodash';
import store from '../store';

export default class Background {

    constructor() {
        this.listener = this.listener.bind(this);
        chrome.runtime.onMessage.addListener(this.listener);
    }

    sendMessage(action, data) {

        if(typeof action !== 'string') {
            throw "Argument action must be a string";
        }

        let request = {};
        request.action = action;
        request.data = data || null;
        chrome.runtime.sendMessage(request);
    }

    listener(request, sender, callback) {

        let state = {};

        switch (request.action) {
            case 'account_address':
                state.type = "SET_ACCOUNT_ADDRESS";
                state.account_address = request.data;
                break;
            case 'report_url':
                state.type = "SET_REPORT_URL";
                state.report_url = request.data;
                break;
            case 'scan_result':
                state.type = "SET_SCAN_RESULT";
                state.scan_result = JSON.stringify(request.data);
                break;
            case 'change_page':
                state.type = 'SET_PAGE';
                state.page = request.data.page;
                break;
        }

        if(!_.isEmpty(state)) {
            store.dispatch(state);
        }
        callback('HI! TEST');
    }
}