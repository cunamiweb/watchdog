export default class ChromeExtension {

    onUpdateAvailable(callback) {
        return chrome.runtime.onUpdateAvailable.addListener(callback);
    }

    onInstalled(callback) {
        return chrome.runtime.onInstalled.addListener(callback);
    }

    onMessage(callback) {
        return chrome.extension.onMessage.addListener(callback);
    }

    onConnect(callback) {
        return chrome.extension.onConnect.addListener(callback);
    }

    onActivated(callback) {
        return chrome.tabs.onActivated.addListener(callback);
    }

    onUpdated(callback) {
        return chrome.tabs.onUpdated.addListener(callback);
    }

    getSelected(windowId, callback) {
        return chrome.tabs.getSelected(callback);
    }

    setIcon(icon) {
        return chrome.browserAction.setIcon(icon);
    }

    getStorage(key, callback) {
        chrome.storage.local.get(callback);
    }

    setStorage(prop) {
        return chrome.storage.local.set(prop);
    }

    setPopup(popup) {
        let set = {};
        // set.popup = popup || "popup.html";
        chrome.browserAction.setPopup(set);
        return this;
    }

    sendRequest(action, data, callback) {

        if (typeof action !== 'string') {
            throw "Argument action must be a string";
        }

        let request = {};
        request.action = action;
        request.data = data || null;

        chrome.runtime.sendMessage(request, callback);
        return this;
    }
}