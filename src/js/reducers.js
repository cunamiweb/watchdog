import {Map} from 'immutable';

export default function reducers(state = Map(), action) {

    switch (action.type) {

        case "SET_STATE":
            state = state.merge(action.state);
            break;

        case "SET_PAGE":
            state = state.setIn(['page'], action.page);
            break;

        case "SET_ACCOUNT_ADDRESS":
            state = state.setIn(['account_address'], action.account_address);
            break;

        case "SET_SCAN_RESULT":
            state = state.setIn(['scan_result'], action.scan_result);
            break;

        case "SET_REPORT_URL":
            state = state.setIn(['report_url'], action.report_url);
            break;

        case "SET_PROTECTION":
            state = state.setIn(['protection'], action.protection);
            break;
    }

    if (action.type !== '@@redux/INIT') {
        let snapshot = JSON.stringify(state.toJSON());
        localStorage.setItem('state', snapshot);
    }
    return state;
};