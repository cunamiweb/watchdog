import ChromeExtension from './classes/ChromeExtension';

function isJSON(str) {
    try {
        let obj = JSON.parse(str);
        if (obj && typeof obj === 'object' && obj !== null) {
            return true;
        }
    } catch (err) {
    }
    return false;
}

function getUrlParts(fullyQualifiedUrl) {

    var url, tempProtocol;
    var a = document.createElement('a');

    if (fullyQualifiedUrl.indexOf('://') == -1) {
        tempProtocol = 'https://';
        a.href = tempProtocol + fullyQualifiedUrl;
    } else {
        a.href = fullyQualifiedUrl;
    }
    url = a.hostname.replace("www.", "");

    return url != 'newtab' ? url : '';
}

function getScanStatus(array, status) {

    return array.statuses.find(function (obj) {
        return obj === status;
    });
}

function setIconWithIteration(iconColor) {

    let color = iconColor == 'none' ? 'yellow' : iconColor;

    const ex = new ChromeExtension();

    for (let i = 0; i < 20; i++) {

        setTimeout(function timer() {
            if (i % 2 === 0) {
                ex.setIcon({
                    path: {
                        "19": "assets/i/icons/19x19_png_" + color + ".png",
                        "38": "assets/i/icons/19x19_png_" + color + ".png"
                    }
                });
            } else {
                ex.setIcon({
                    path: {
                        "19": "assets/i/wd_128x128.png",
                        "38": "assets/i/wd_128x128.png"
                    }
                });
            }
        }, i * 150);
    }
}

function scanResult(data) {

    if (data.reportStatuses.approved !== undefined && data.legitStatuses.approved !== undefined) {
        return 'yellow';
    } else if (data.reportStatuses.approved !== undefined) {
        return 'red';
    } else if (data.legitStatuses.approved !== undefined) {
        return 'blue';
    } else if (data.reportStatuses.pending) {
        return 'yellow';
    } else if (data.legitStatuses.pending) {
        return 'yellow';
    } else {
        return 'none';
    }
}

export {isJSON, getUrlParts, getScanStatus, scanResult, setIconWithIteration};