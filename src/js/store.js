import _ from 'lodash';
import {createStore, applyMiddleware} from 'redux';
import reducers from './reducers';
import middleware from './middleware';
import {isJSON} from './helpers';

//const store = createStore(reducers);

const createStoreWithMiddleware = applyMiddleware(
    middleware
)(createStore);

const store = createStoreWithMiddleware(reducers);

// localStorage.clear();
let state = localStorage.getItem('state');

if (isJSON(state)) {
    state = JSON.parse(state);
}

if (_.isNull(state) || _.isEmpty(state)) {
    state = {
        page: 'home',
        account_address: null,
        scan_result: null,
        protection: true
    }
}

store.dispatch({
    type: "SET_STATE",
    state: state
});

module.exports = store;