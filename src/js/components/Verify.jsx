import React from 'react';
import Header from './Header.jsx';

import {connect} from 'react-redux';

class Verify extends React.Component {
    render() {
        return (
            <div className="content">
                <Header/>
                <img className="img_center mb-1" src="assets/i/question-icon.png" alt=""/>
                <h1 className="content__title mb-1">this website is not verified</h1>
                <div className="content__subtitle">https://someproject.io</div>

                <form className="form" action="/" method="post">
                    <div className="form__group">
                        <label className="form__label" htmlFor="verify_type">Choose entity type</label>
                        <div className="form__select">
                            <select name="verify_type" id="verify_type">
                                <option value="1">Web project</option>
                                <option value="2">Option 2</option>
                                <option value="3">Option 3</option>
                            </select>
                        </div>
                    </div>
                    <div className="form__group">
                        <label className="form__label" htmlFor="verify_url">Domain name of legit entity</label>
                        <input className="form__input" name="verify_url" id="verify_url" type="url"/>
                    </div>
                    <div className="form__group">
                        <label className="form__label" htmlFor="verify_email">E-mail of project founders</label>
                        <input className="form__input" name="verify_email" id="verify_email" type="email"/>
                    </div>
                    <button className="btn btn--red btn--main" type="submit">submit and get rewards</button>
                </form>
            </div>
        );
    }
}


module.exports = connect(
    (state) => {


    },
    (dispatch) => {

    }
)(Verify);