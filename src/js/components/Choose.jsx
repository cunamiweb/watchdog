import React from 'react';
import Header from './Header.jsx';

import {connect} from 'react-redux';
import {setPage} from '../actions';

class Choose extends React.Component {
    render() {
        return (
            <div className="content">
                <Header/>
                <h1 className="content__title">Choose report type</h1>
                <div className="form__group text-center">
                    <a onClick={() => this.props.setPage('report')} className="btn btn--red btn--choose">Report Scam</a>
                </div>
                <div className="form__group text-center">
                    <a className="btn btn--gray btn--choose">Add to legit database</a>
                </div>
                <div className="trusted">
                    <div className="trusted__title">trusted by</div>
                    <img className="img_center" src="assets/i/cryptopolice-logo.png" alt=""/>
                </div>
            </div>
        );
    }
}

module.exports = connect(
    function mapStateToProps(state) {
        return {
            page: state.get('page').toString()
        };

    }, function mapDispatchToProps(dispatch) {
        return {
            setPage: page => dispatch(setPage(page))
        };
    }
)(Choose);