import React from 'react';

import {connect} from "react-redux";

export default class Content extends React.Component {
    constructor(props) {
        super(props);
        // Bind the method to the component context
        this.renderChildren = this.renderChildren.bind(this);
        this.onSetPage = this.onSetPage.bind(this);
    }

    onSetPage(newPage) {
        this.setState({page: newPage});
    }

    renderChildren() {
        return React.Children.map(this.props.children, child => {
            if (child.key === this.props.page) {
                return child;
            }
        })
    }

    render() {
        return this.renderChildren()
    }
};


module.exports = connect(function mapStateToProps(state) {
    return {
        page: state.get('page').toString()
    };
})(Content);