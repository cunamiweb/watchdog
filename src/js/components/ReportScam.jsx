import React from 'react';
import Header from './Header.jsx';

import {connect} from 'react-redux';
import {setPage, setReportUrl} from "../actions";

export default class ReportScam extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            domain: this.props.report_url,
            category: 1,
            comment: ''
        }
    };

    handleSubmit = (e) => {
        e.preventDefault();
        window.open('https://dapp.cryptopolice.com/new-report?' + Object.keys(this.state).map(key => key + '=' + this.state[key]).join('&'), '_blank');
    };

    handleChange = (e) => {
        let state = {};
        state[e.target.name] = e.target.value;
        this.setState(state);
    };

    render() {
        return (
            <div className="content">
                <Header/>
                <h1 className="content__title">report scam</h1>
                <form className="form" onSubmit={this.handleSubmit} method="post">
                    <div className="form__group">
                        <label className="form__label" htmlFor="report_type">Choose scam type</label>
                        <div className="form__select">
                            <select name="category" onChange={this.handleChange} id="report_type">
                                <option style={{color: 'black'}} key="1" value="1">Phishing</option>
                                <option style={{color: 'black'}} key="2" value="2">Investment scams</option>
                                <option style={{color: 'black'}} key="3" value="3">Fake project</option>
                            </select>
                        </div>
                    </div>
                    <div className="form__group">
                        <label className="form__label" htmlFor="report_url">Domain name where SCAM is running</label>
                        <input className="form__input" name="domain" id="report_url" type="text" onChange={this.handleChange} value={this.state.domain || ''}/>
                    </div>
                    <div className="form__group">
                        <label className="form__label" htmlFor="report_text">Your comment (In English)</label>
                        <textarea className="form__textarea" name="comment" id="report_text" onChange={this.handleChange} value={this.state.comment || ''}> </textarea>
                    </div>
                    <button className="btn btn--red btn--main" type="submit">go further</button>
                </form>
            </div>
        );
    }
};

module.exports = connect(
    function mapStateToProps(state) {
        return {
            report_url: state.get('report_url'),
            page: state.get('page').toString()
        };
    }, function mapDispatchToProps(dispatch) {
        return {
            setPage: page => dispatch(setPage(page)),
            setReportUrl: report_url => dispatch(setReportUrl(report_url))
        };
    }
)(ReportScam);