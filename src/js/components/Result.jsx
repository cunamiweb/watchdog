import React from 'react';
import Header from './Header.jsx';
import {scanResult} from "../helpers";
import {setPage, setScanResult} from "../actions";

import {connect} from 'react-redux';

class Result extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        let link;
        let statusTitle;
        let srcPath = 'assets/i/';
        let decoded_data = JSON.parse(this.props.scan_result);
        let scan_result = scanResult(decoded_data);

        switch (scan_result) {
            case 'yellow' :
                srcPath += 'warning';
                statusTitle = 'VERIFICATION IN PROGRESS';
                link = <a href={'https://dapp.cryptopolice.com/table/' + decoded_data.currentUrl + '?utm_source=extension'} className="btn btn--red btn--main" target="_blank">See progress</a>;
                break;
            case 'blue' :
                srcPath += 'legit';
                statusTitle = 'THIS WEBSITE IS LEGIT';
                link = <a href={'https://dapp.cryptopolice.com/table/' + decoded_data.currentUrl + '?utm_source=extension'} className="btn btn--red btn--main" target="_blank">Get proofs</a>;
                break;
            case 'red' :
                srcPath += 'scam';
                statusTitle = 'THIS WEBSITE IS A SCAM';
                link = <a href={'https://dapp.cryptopolice.com/table/' + decoded_data.currentUrl  + '?utm_source=extension'} className="btn btn--red btn--main" target="_blank">Get proofs</a>;
                break;
            default:
                srcPath += 'warning';
                statusTitle = 'THIS WEBSITE IS NOT VERIFIED YET';
                link = <a onClick={() => this.props.setPage('choose')} className="btn btn--red btn--main">Submit report</a>;
                break;
        }

        srcPath += '-icon.png';

        return (
            <div className="content">
                <Header/>
                <h1 className="content__title">{statusTitle}</h1>
                <div className="content__subtitle">{decoded_data.currentUrl}</div>
                <img className="img_center" src={srcPath} alt=""/>
                <div className="trusted">
                    <a className="trusted_link" href="https://www.cryptopolice.com/" target="_blank">
                        <div className="trusted__title">trusted by</div>
                        <img className="img_center" src="assets/i/cryptopolice-logo.png" alt=""/>
                    </a>
                </div>
                {link}
            </div>
        );
    }
}

module.exports = connect(
    function mapStateToProps(state) {
        return {
            scan_result: state.get('scan_result'),
            page: state.get('page').toString()
        };
    }, function mapDispatchToProps(dispatch) {
        return {
            setPage: page => dispatch(setPage(page)),
            setScanResult: scan_result => dispatch(setScanResult(scan_result))
        };
    }
)(Result);
