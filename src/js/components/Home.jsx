import React from 'react';
import Header from './Header.jsx';

import {connect} from 'react-redux';
import {setPage} from '../actions';

class Home extends React.Component {
    render() {
        return (
            <div className="content">
                <Header/>
                <h1 className="content__title thin small">Welcome To</h1>
                <img className="img_center" src="assets/i/watchdog-logo.png" width="153" alt="Watchdog"/>
                <div className="learn">
                    <div className="learn__item">
                        <a className="learn__link" target="_blank"
                           href="https://medium.com/cryptopolice/introducing-demo-version-of-watchdog-f595fd82c2b9">
                            <span>learn how it works</span>
                            <img className="img_center" src="assets/i/settings-icon.png" alt="learn how it works"/>
                        </a>
                    </div>
                    <div className="learn__item">
                        <a className="learn__link" target="_blank" href="https://cryptopolice.com/">
                            <span>powered by</span>
                            <img className="img_center" src="assets/i/cryptopolice-logo.png" alt="Cryptopolice"/>
                        </a>
                    </div>
                </div>
                <a onClick={() => this.props.setPage('choose')} className="btn btn--red btn--main">submit report</a>
            </div>
        );
    }
}

module.exports = connect(
    function mapStateToProps(state) {
        return {
            page: state.get('page').toString()
        };
    }, function mapDispatchToProps(dispatch) {
        return {
            setPage: page => dispatch(setPage(page))
        };
    }
)(Home);