import React from 'react';
import _ from 'lodash';
import {setPage} from "../actions";
import {connect} from "react-redux";
import makeBlockie from 'ethereum-blockies-base64';

export default class Header extends React.Component {
    render() {

        let src = makeBlockie(this.props.account_address);

        return <div className="header">
            <div className="header__item">
                {/*<div className="header__avatar">*/}
                    {/*<a onClick={() => this.props.setPage('profile')}>*/}
                        {/*<img src={src} width="35" height="35" alt="Username"/>*/}
                    {/*</a>*/}

                    {/*<div>*/}
                        {/*Hello guest,<br/>please <a href="#">{this.props.account_address.substring(0, 5)}...</a>*/}
                    {/*</div>*/}
                {/*</div>*/}

                <a className="header__back" onClick={() => this.props.setPage('home')}>
                    <img src="assets/i/back-icon.png" alt=""/>
                </a>
            </div>

            <div className="header__item">
                {/*<div className="header__balance">*/}
                    {/*<a onClick={() => this.props.setPage('profile')}>*/}
                        {/*<span>123121</span> OFCR*/}
                    {/*</a>*/}
                {/*</div>*/}
            </div>
        </div>
    }
};

module.exports = connect(
    function mapStateToProps(state) {

        let addr = state.get('account_address');


        if (_.isNull(addr) || _.isUndefined(addr)) {
            addr = '??????????????????????????????????????????';
        } else {
            addr = addr.toString();
        }

        return {
            account_address: addr
        };

    }, function mapDispatchToProps(dispatch) {
        return {
            setPage: page => dispatch(setPage(page))
        };
    }
)(Header);