import React from 'react';
import {setPage, setProtection} from "../actions";
import {connect} from "react-redux";
import Header from "./Header";

export default class Footer extends React.Component {

    handleChange = (e) => {

        let status = !this.props.protection;
        this.props.setProtection(status);
        chrome.storage.local.set({protection_status: status});

        if (!status) {
            this.props.setPage('home');
        }

    };

    render() {
        return <div className="footer">
            <div className="switcher">
                <input className="switcher__input"
                       onChange={this.handleChange}
                       checked={this.props.protection}
                       type="checkbox" name="protection"
                       id="protection"
                       value={this.props.protection}/>
                <label className="switcher__label" htmlFor="protection">
                    <span className="switcher__text">protection <br/>activated</span>
                </label>
            </div>
            <a href="https://docs.google.com/forms/d/e/1FAIpQLSdEiSKHRGUvGVJ6o8eYMn56zU6f6xfs-hCv8BHEZ3qNeCkuvw/viewform"
               target="_blank" className="footer__status">send feedback</a>
        </div>
    }
}


module.exports = connect(
    function mapStateToProps(state) {
        return {
            protection: state.get('protection')
        };

    }, function mapDispatchToProps(dispatch) {
        return {
            setPage: page => dispatch(setPage(page)),
            setProtection: protection => dispatch(setProtection(protection))
        };
    }
)(Footer);