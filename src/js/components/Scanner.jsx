import React from 'react';
import Header from './Header.jsx';

import {connect} from 'react-redux';


class Scanner extends React.Component {
    render() {
        return (
            <div className="content">
                <Header/>
                <form className="search" action="/" method="post">
                    <input className="search__input" name="search" type="text"/>
                    <input className="search__btn" type="submit" name="search_btn"/>
                </form>

                <div className="search__founded">Founded 56 results</div>

                <ul className="search__results">
                    <li className="legit">
                        <span>Lorem.io</span>
                        <a href="#">View report</a>
                    </li>
                    <li className="warning">
                        <span>Lorem.io</span>
                        <a href="#">View report</a>
                    </li>
                    <li className="legit">
                        <span>Lorem.io</span>
                        <a href="#">View report</a>
                    </li>
                    <li className="warning">
                        <span>Lorem.io</span>
                        <a href="#">View report</a>
                    </li>
                    <li className="legit">
                        <span>Lorem.io</span>
                        <a href="#">View report</a>
                    </li>
                    <li className="warning">
                        <span>Lorem.io</span>
                        <a href="#">View report</a>
                    </li>
                    <li className="legit">
                        <span>Lorem.io</span>
                        <a href="#">View report</a>
                    </li>
                    <li className="warning">
                        <span>Lorem.io</span>
                        <a href="#">View report</a>
                    </li>
                    <li className="legit">
                        <span>Lorem.io</span>
                        <a href="#">View report</a>
                    </li>
                    <li className="warning">
                        <span>Lorem.io</span>
                        <a href="#">View report</a>
                    </li>
                    <li className="legit">
                        <span>Lorem.io</span>
                        <a href="#">View report</a>
                    </li>
                    <li className="warning">
                        <span>Lorem.io</span>
                        <a href="#">View report</a>
                    </li>
                    <li className="legit">
                        <span>Lorem.io</span>
                        <a href="#">View report</a>
                    </li>
                    <li className="warning">
                        <span>Lorem.io</span>
                        <a href="#">View report</a>
                    </li>
                    <li className="legit">
                        <span>Lorem.io</span>
                        <a href="#">View report</a>
                    </li>
                    <li className="warning">
                        <span>Lorem.io</span>
                        <a href="#">View report</a>
                    </li>
                    <li className="legit">
                        <span>Lorem.io</span>
                        <a href="#">View report</a>
                    </li>
                    <li className="warning">
                        <span>Lorem.io</span>
                        <a href="#">View report</a>
                    </li>
                </ul>

            </div>);
    }
}

module.exports = connect(
    function mapStateToProps(state) {
        return {
            page: state.get('page').toString()
        };

    }, function mapDispatchToProps(dispatch) {
        return {
            setPage: page => dispatch(setPage(page))
        };
    }
)(Scanner);