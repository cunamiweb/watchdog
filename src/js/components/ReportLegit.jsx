import React from 'react';
import Header from './Header.jsx';

export default class ReportScam extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            reportUrl: this.props.reportUrl,
            reportType: null,
            reportText: null
        };
    }

    handleSubmit = (e) => {
        e.preventDefault();
    };

    handleChange = (e) => {
        let state = {};
        state[e.target.name] = e.target.value;
        this.setState(state);
    };

    render() {
        return (
            <div className="content">
                <Header/>
                <h1 className="content__title">Add leggit entity</h1>

                <ul className="search__results" style={{height: '300px'}}>
                    <form className="form" id="legit_form" action="/" method="post">
                        <div className="form__group">
                            <label className="form__label" htmlFor="report_type">Choose legit type</label>
                            <div className="form__select">
                                <select name="legit_type" id="legit_type">
                                    <option style={{color: 'black'}} value="website">Website</option>
                                    <option style={{color: 'black'}} value="account">Account</option>
                                </select>
                            </div>
                        </div>
                        <div className="form__group">
                            <label className="form__label" htmlFor="report_type">you are the owner?</label>
                            <div className="radio">
                                <input className="radio__input" type="radio" name="owner" id="radio_1" value="yes"/>
                                <label className="radio__label" htmlFor="radio_1">
                                    Yes
                                </label>
                            </div>
                            <div className="radio">
                                <input className="radio__input" type="radio" name="owner" id="radio_2" value="no"/>
                                <label className="radio__label" htmlFor="radio_2">
                                    No
                                </label>
                            </div>
                        </div>
                        <div className="form__group">
                            <label className="form__label" htmlFor="report_url">Domain name where SCAM is
                                running</label>
                            <input className="form__input" name="report_url" id="report_domain_url_1"
                                   placeholder="No domain detected in your browser" required=""/>
                        </div>
                        <div className="form__group">
                            <label className="form__label" htmlFor="email">Official email address</label>
                            <input type="email" name="email_address" className="form__input" placeholder="Email address"
                                   required=""/>
                        </div>
                        <div className="form__group">
                            <label className="form__label" htmlFor="report_text">Your comment (In English)</label>
                            <textarea className="form__textarea" name="report_text" id="report_text"> </textarea>
                        </div>
                        <button className="btn btn--red btn--main" id="report_add" type="submit">
                            <div className="btn_text">go further</div>
                        </button>
                    </form>
                </ul>
            </div>
        );
    }
};